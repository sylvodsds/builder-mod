# Builder Mod

This is the mod for the *Webliero Builder Room*

it started as a "full blown" mod, but this is a split-up version to add brick sets easily and allow the [builder room's script](https://github.com/s-dsds/builder-room) to add new weapons on the fly

[https://sylvodsds.gitlab.io/builder-mod/](https://sylvodsds.gitlab.io/builder-mod/)